import telebot
import schedule
import pandas as pd
import sys
from threading import Thread
from time import sleep
from datetime import datetime, date
from dateutil.relativedelta import *

if len(sys.argv) > 1:
    token = sys.argv[1]
else:
    print("No token passed")

bot = telebot.TeleBot(token)

global credit_file_loc
credit_file_loc = "credit.xlsx"

def today():
    today = datetime.now()
    today = datetime.date(today)
    return today


date_format = "%d/%m/%Y"
global month_number
month_number = today().month
month_name = today().strftime('%B')
print(f"Month number is: {month_number}")


def date_from_excel():
    df_excel = pd.read_excel(credit_file_loc, usecols="A", skiprows=0, skipfooter=0, header=0)
    result: date
    result = []
    for index, row in df_excel.head(1000).itertuples():
        row: date
        row = datetime.date(row)
        result.append(row)
    return result

def row_pay_from_excel():
    df_excel = pd.read_excel(credit_file_loc, usecols="A,B", skiprows=0, skipfooter=0, header=0)
    for index, row in df_excel.head(1000).iterrows():
        for col in df_excel.head(1000):
            if row[col] == pd.Timestamp(today()):
                return row["Payment"]


def balance_from_excel():
    df_excel = pd.read_excel(credit_file_loc, usecols="A,B,C", skiprows=0, skipfooter=0, header=0)
    for index, row in df_excel.head(1000).iterrows():
        for col in df_excel.head(1000):
            if (row[col] == pd.Timestamp(today())) or (row[col] == row_pay_from_excel()):
                return row["Balance"]


def month_balance_from_excel():
    df_excel = pd.read_excel(credit_file_loc, usecols="A,B,C", skiprows=0, skipfooter=0, header=0)
    for index, row in df_excel.head(1000).iterrows():
        for col in df_excel.head(1000):
            if (type(row[col]) is pd.Timestamp) and (row[col].month == today().month) and (row[col].day <= today().day):
                return row["Balance"]
            elif (type(row[col]) is pd.Timestamp) and (row[col].month == today().month) and (row[col].day > today().day):
                row_last = row[col] + relativedelta(months=-1)
                for index, row_last in df_excel.head(1000).iterrows():
                    print(row_last["Balance"])
                    return row_last["Balance"]


print(month_balance_from_excel())
print(row_pay_from_excel())
# print(balance_from_excel())

# Command /start
@bot.message_handler(commands=["start"])
def start(m, res=False):
    global chat_id
    chat_id = m.chat.id
    bot.send_message(m.chat.id, 'Hello!')


now = datetime.now()
print(now.time())
# print(date_from_excel())

def schedule_checker():
    while True:
        schedule.run_pending()
        sleep(1)


def send_payment_day():
    if today() in date_from_excel():
        return bot.send_message(chat_id, "Today is payment day: " + str(today()) + "! To pay: " + str(row_pay_from_excel()) + "RUB")
    else:
        return bot.send_message(chat_id, "Don't pay today!   " + str(today()))


if __name__ == "__main__":
    # Create the job in schedule.
    schedule.every().day.at("09:00").do(send_payment_day)
    Thread(target=schedule_checker).start()


@bot.message_handler(commands=["today"])
def send_today(m, res=False):
    try:
        bot.send_message(m.chat.id, "Today is :   " + str(today()))
    except:
        bot.send_message(m.chat.id, "Invalid input")


@bot.message_handler(commands=["row"])
def send_row(m, res=False):
    try:
        bot.send_message(m.chat.id, "Row (date from excel) :   " + str(date_from_excel()))
    except:
        bot.send_message(m.chat.id, "Invalid input")


@bot.message_handler(commands=["build"])
def send_row(m, res=False):
    try:
        bot.send_message(m.chat.id, "Info:" + " BUILDCOMMITTAGS ")
    except:
        bot.send_message(m.chat.id, "Invalid input")        


@bot.message_handler(commands=["balance"])
def send_row(m, res=False):
    try:
        bot.send_message(m.chat.id, "Balance: " + str(month_balance_from_excel()) + "RUB. Month: " + month_name)
    except:
        bot.send_message(m.chat.id, "Invalid input")   


bot.polling(none_stop=True, interval=0)
