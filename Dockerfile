FROM nickgryg/alpine-pandas:3.7
WORKDIR /app
COPY /app .
COPY requirements.txt .
ENV TZ=Europe/Samara
RUN apk update && apk upgrade
RUN apk add --no-cache bash\
                        gcc \
                        libcurl \
                        libc-dev \
                        g++ \
                        snappy-dev \
                        snappy \
                        tzdata \
    && rm -rf /var/cache/apk/*
RUN pip3 install --upgrade pip
RUN pip3 install -r requirements.txt
RUN ln -snf /usr/share/zoneinfo/$TZ /etc/localtime && echo $TZ > /etc/timezone
ENTRYPOINT ["python3", "-u", "./credit-bot.py"]
